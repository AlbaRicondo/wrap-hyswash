# Wrap HySwash

A python wrap for SWASH numerical model.


## Table of contents
1. [Description](#desc)
2. [Main Contents](#mc)
3. [Documentation](#doc)
4. [Schemes](#sch)
5. [Install](#ins)
    1. [Install from sources](#ins_src)
    2. [Install SWASH numerical model](#ins_swh)
6. [Examples](#exp)
7. [Contributors](#ctr)
8. [License](#lic)


<a name="desc"></a>
## Description


<a name="mc"></a>
## Main contents

[hyswash](./hyswash): SWASH numerical model wrapper 
- [notebooks](./hyswash/notebooks)
[statistical_toolkit](./statistical_toolkit)
- [bluemathtk](.statistical_toolkit/bluemathtk)
	- [MDA](.statistical_toolkit/bluemathtk/MDA.py)
	- [PCA](.statistical_toolkit/bluemathtk/PCA.py)
	- [RBF](.statistical_toolkit/bluemathtk/RBF.py)
[wrap_swash](./wrap_swash): SWASH numerical model wrapper 
- [wswash](./wrap_swash/wswash)
	- [io](./wrap_swash/wswash/io.py): SWASH numerical model input/output operations
	- [plots](./wrap_swash/wswash/plots.py): plotting module 
	- [postprocessor](./wrap_swash/wswash/postprocessor.py): SWASH output postprocessor module
	- [profiles](./wrap_swash/wswash/profiles.py): customized bathymetry profiles library 
	- [spectra](./wrap_swash/wswash/spectra.py): spectral analysis for output postprocessing
	- [statistics](./wrap_swash/wswash/statistics.py): statistical analysis for output postprocessing 
	- [waves](./wrap_swash/wswash/waves.py): waves series generation library 
	- [wrap](./wrap_swash/wswash/wrap.py): SWASH numerical model python wrap 


<a name="doc"></a>
## Documentation

SWASH numerical model detailed documentation can be found at: <http://swash.sourceforge.net/>

- [SWASH install/compile manual](http://swash.sourceforge.net/download/download.htm)
- [SWASH user manual](http://swash.sourceforge.net/online_doc/swashuse/swashuse.html)


<a name="sch"></a>
## Schemes

<a name="ins"></a>
## Download Executables
- - -

The last official SWASH Delft execs:  <https://sourceforge.net/projects/swash/files/swash/> 

* Only Windows and Linux executables are available, MacOS users should compile it using the 'tar' file.

<a name="ins"></a>
## Install
- - -

Source code is currently privately hosted on GitLab at:  <https://gitlab.com/AlbaRicondo/wrap-hyswash> 


<a name="ins_src"></a>
### Install from sources

Install requirements. Navigate to the base root of [wrap-hyswash\](./) and execute:

```bash
   conda env create -f environment.yml

```

<a name="ins_swh"></a>
### Install SWASH numerical model 

Download and Compile SWASH numerical model:

```bash
  # you may need to install a fortran compiler
  - For Linux:
    sudo apt install gfortran

  - For MacOS:
    brew install gcc

  # download and unpack
  wget https://swash.sourceforge.io/download/zip/swash-7.01AB.tar.gz
  tar -zxvf swash-7.01AB.tar.gz

  # compile numerical model
  cd swash/
  make config
  make ser
```

Copy SWASH binary file to module resources

```bash
  # Launch a python interpreter
  $ python

  Python 3.6.9 (default, Apr 18 2020, 01:56:04) 
  [GCC 8.4.0] on linux
  Type "help", "copyright", "credits" or "license" for more information.
  
  >>> import wswash 
  >>> wswash.set_swash_binary_file('swash.exe')
```

<a name="ctr"></a>
## Contributors:

Alba Ricondo Cueva (ricondoa@unican.es)\
Fernando Mendez Incera (fernando.mendez@unican.es)


<a name="lic"></a>
## License

This project is licensed under the MIT License - see the [license](./LICENSE.txt) file for details

