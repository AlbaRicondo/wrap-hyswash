# HySwash

A stadistical-dynamical donwscaling method to predict surf-zone hydrodynamics over 1D coral-reef profiles. A  manegable ecosystem integrated in Python.

## Table of contents
1. [Description](#desc)
2. [Main Contents](#mc)
3. [Documentation](#doc)
4. [Schemes](#sch)
5. [Install](#ins)
    1. [Install from sources](#ins_src)
    2. [Install SWASH numerical model](#ins_swh)
6. [Examples](#exp)
7. [Contributors](#ctr)
8. [License](#lic)

<a name="desc"></a>
## Description

The purpose of this notebook is to provide the user a tool to ease the integration of statistical (i.e, LHS, MDA, PCA, RBF) and numerical models (SWASH) to efficiently predict the nearshore wave trasnformation along cross-shore profiles. This hybrid approach is a combination of: 
(1) selection of N representative wind-wave states of the wave-hindcast
(2) numerical simulation of the N states with the open-source numerical model 1D SWASH
(3) nearshore reconstruction of wave overtopping volume and run-up excursion as a proxy for the flooding events. 
The simplicity of the commands inputs make the notebook suitable for performing different sensitivity analysis. 

<a name="mc"></a>
## Main contents

[wswash](./wswash): SWASH numerical model wrapper 
- [io](./wswash/io.py): SWASH numerical model input/output operations
- [plots](./wswash/plots.py): plotting module 
- [postprocessor](./wswash/postprocessor.py): SWASH output postprocessor module
- [profiles](./wswash/profiles.py): customized bathymetry profiles library 
- [spectra](./wswash/spectra.py): spectral analysis for output postprocessing
- [statistics](./wswash/statistics.py): statistical analysis for output postprocessing 
- [waves](./wswash/waves.py): waves series generation library 
- [wrap](./wswash/wrap.py): SWASH numerical model python wrap 

<a name="doc"></a>
## Documentation

SWASH (Simulating WAves till SHore) is intented to be a free-surface, terrain-following, multi-dimensional hydrodynamic simulation model used to predict transformation of surface waves and rapidly varied shallow water flows in coastal waters. It solves the continuity and momentum equations, and optionally the equations for conservative transport of salinity, temperature and suspended load for both noncohesive sediment (e.g. sand) and cohesive sediment (mud, clay, etc.) (Zilema et al. 2008).

For more information about the SWASH model, consult the implementation manual that can be found at <a href="http://swash.sourceforge.net/" target="_blank">SWASH home page.

![HySwash Modules](Modules_Map.drawio.png)

<a name="lic"></a>
## License

This project is licensed under the MIT License - see the [license](./LICENSE.txt) file for details

